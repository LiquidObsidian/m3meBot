﻿Public Module Config
    Public Aimbot_Enabled As Boolean = True
    Public Aimbot_Key As Integer = 18
    Public Aimbot_Smooth As Boolean = False
    'Public Aimbot_InCross As Boolean = False
    Public Aimbot_FOV As Single = 45
    Public Aimbot_SmoothSpeed As Single = 10
    Public Aimbot_AutoShoot As Boolean = True

    Public Trigger_Enabled As Boolean = True
    Public Trigger_Key As Integer = 6
    Public Trigger_Delay As Integer = 0

    Public Glow_Team_Enabled As Boolean = True
    Public Glow_Enemy_Enabled As Boolean = True
    Public Glow_Team_Rainbow As Boolean = False
    Public Glow_Enemy_Rainbow As Boolean = False

    Public Glow_Alpha As Integer = 255
    Public Glow_Team_R As Integer = 0
    Public Glow_Team_G As Integer = 255
    Public Glow_Team_B As Integer = 0
    Public Glow_Enemy_R As Integer = 255
    Public Glow_Enemy_G As Integer = 0
    Public Glow_Enemy_B As Integer = 0

    Public Misc_Bhop_Enabled As Boolean = True
    Public Misc_Radar_Enabled As Boolean = True
    Public Misc_NoFlash_Enabled As Boolean = True
    'Public Misc_SlowAim_Enabled As Boolean = True
    'Public Misc_SlowAim_Sensitivity As Single = 1.0F
    Public Misc_RapidFire_Enabled As Boolean = True
    Public Misc_RapidFire_Key As Integer = 5

    Public Sub Save()
        Try
            Ini.Write("Aimbot_Enabled", Aimbot_Enabled)
            Ini.Write("Aimbot_Key", Aimbot_Key)
            Ini.Write("Aimbot_Smooth", Aimbot_Smooth)
            'Ini.Write("Aimbot_InCross", Aimbot_InCross)
            Ini.Write("Aimbot_FOV", Aimbot_FOV)
            Ini.Write("Aimbot_SmoothSpeed", Aimbot_SmoothSpeed)
            Ini.Write("Aimbot_AutoShoot", Aimbot_AutoShoot)
            Ini.Write("Trigger_Enabled", Trigger_Enabled)
            Ini.Write("Trigger_Key", Trigger_Key)
            Ini.Write("Trigger_Delay", Trigger_Delay)
            Ini.Write("Glow_Team_Enabled", Glow_Team_Enabled)
            Ini.Write("Glow_Enemy_Enabled", Glow_Enemy_Enabled)
            Ini.Write("Glow_Team_Rainbow", Glow_Team_Rainbow)
            Ini.Write("Glow_Enemy_Rainbow", Glow_Enemy_Rainbow)
            Ini.Write("Glow_Alpha", Glow_Alpha)
            Ini.Write("Glow_Team_R", Glow_Team_R)
            Ini.Write("Glow_Team_G", Glow_Team_G)
            Ini.Write("Glow_Team_B", Glow_Team_B)
            Ini.Write("Glow_Enemy_R", Glow_Enemy_R)
            Ini.Write("Glow_Enemy_G", Glow_Enemy_G)
            Ini.Write("Glow_Enemy_B", Glow_Enemy_B)
            Ini.Write("Misc_Bhop_Enabled", Misc_Bhop_Enabled)
            Ini.Write("Misc_Radar_Enabled", Misc_Radar_Enabled)
            Ini.Write("Misc_NoFlash_Enabled", Misc_NoFlash_Enabled)
            'Ini.Write("Misc_SlowAim_Enabled", Misc_SlowAim_Enabled)
            'Ini.Write("Misc_SlowAim_Sensitivity", Misc_SlowAim_Sensitivity)
            Ini.Write("Misc_RapidFire_Enabled", Misc_RapidFire_Enabled)
            Ini.Write("Misc_RapidFire_Key", Misc_RapidFire_Key)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Load()
        Try
            Aimbot_Enabled = Ini.Read("Aimbot_Enabled")
            Aimbot_Key = Ini.Read("Aimbot_Key")
            Aimbot_Smooth = Ini.Read("Aimbot_Smooth")
            'Aimbot_InCross = Ini.Read("Aimbot_InCross")
            Aimbot_FOV = Ini.Read("Aimbot_FOV")
            Aimbot_AutoShoot = Ini.Read("Aimbot_AutoShoot")
            Aimbot_SmoothSpeed = Ini.Read("Aimbot_SmoothSpeed")
            Trigger_Enabled = Ini.Read("Trigger_Enabled")
            Trigger_Key = Ini.Read("Trigger_Key")
            Trigger_Delay = Ini.Read("Trigger_Delay")
            Glow_Team_Enabled = Ini.Read("Glow_Team_Enabled")
            Glow_Enemy_Enabled = Ini.Read("Glow_Enemy_Enabled")
            Glow_Team_Rainbow = Ini.Read("Glow_Team_Rainbow")
            Glow_Enemy_Rainbow = Ini.Read("Glow_Enemy_Rainbow")
            Glow_Alpha = Ini.Read("Glow_Alpha")
            Glow_Team_R = Ini.Read("Glow_Team_R")
            Glow_Team_G = Ini.Read("Glow_Team_G")
            Glow_Team_B = Ini.Read("Glow_Team_B")
            Glow_Enemy_R = Ini.Read("Glow_Enemy_R")
            Glow_Enemy_G = Ini.Read("Glow_Enemy_G")
            Glow_Enemy_B = Ini.Read("Glow_Enemy_B")
            Misc_Bhop_Enabled = Ini.Read("Misc_Bhop_Enabled")
            Misc_Radar_Enabled = Ini.Read("Misc_Radar_Enabled")
            Misc_NoFlash_Enabled = Ini.Read("Misc_NoFlash_Enabled")
            'Misc_SlowAim_Enabled = Ini.Read("Misc_SlowAim_Enabled")
            'Misc_SlowAim_Sensitivity = Ini.Read("Misc_SlowAim_Sensitivity")
            Misc_RapidFire_Enabled = Ini.Read("Misc_RapidFire_Enabled")
            Misc_RapidFire_Key = Ini.Read("Misc_RapidFire_Key")
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UpdateConfig()
        Try
            Aimbot_Enabled = m3meBot.AimbotEnabled.Checked
            Aimbot_Key = CInt(m3meBot.AimbotKey.Text)
            Aimbot_Smooth = m3meBot.AimbotSmooth.Checked
            'Aimbot_InCross = m3meBot.AimbotInCross.Checked
            Aimbot_FOV = CSng(m3meBot.AimbotFOV.Value)
            Aimbot_SmoothSpeed = CSng(m3meBot.AimbotSmoothSpeed.Value)
            Aimbot_AutoShoot = m3meBot.AimbotAutoShoot.Checked
            Trigger_Enabled = m3meBot.TriggerEnabled.Checked
            Trigger_Key = CInt(m3meBot.TriggerBotKey.Text)
            Trigger_Delay = CInt(m3meBot.TriggerBotDelay.Text)
            Glow_Team_Enabled = m3meBot.GlowTeam.Checked
            Glow_Enemy_Enabled = m3meBot.GlowEnemy.Checked
            Glow_Team_Rainbow = m3meBot.TeamRainbow.Checked
            Glow_Enemy_Rainbow = m3meBot.EnemyRainbow.Checked
            Glow_Alpha = m3meBot.GlowAlpha.Value
            Glow_Team_R = m3meBot.TeamR.Value
            Glow_Team_G = m3meBot.TeamG.Value
            Glow_Team_B = m3meBot.TeamB.Value
            Glow_Enemy_R = m3meBot.EnemyR.Value
            Glow_Enemy_G = m3meBot.EnemyG.Value
            Glow_Enemy_B = m3meBot.EnemyB.Value
            Misc_Bhop_Enabled = m3meBot.BunnyHop.Checked
            Misc_Radar_Enabled = m3meBot.RadarEnabled.Checked
            Misc_NoFlash_Enabled = m3meBot.NoFlashEnabled.Checked
            'Misc_SlowAim_Enabled = m3meBot.SlowAimEnabled.Checked
            'Misc_SlowAim_Sensitivity = CSng(m3meBot.SlowAimSensitivity.Text)
            Misc_RapidFire_Enabled = m3meBot.RapidFireEnabled.Checked
            Misc_RapidFire_Key = CInt(m3meBot.RapidFireKey.Text)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UpdateMenu()
        Try
            m3meBot.AimbotEnabled.Checked = Aimbot_Enabled
            m3meBot.AimbotKey.Text = Aimbot_Key
            m3meBot.AimbotSmooth.Checked = Aimbot_Smooth
            'm3meBot.AimbotInCross.Checked = Aimbot_InCross
            m3meBot.AimbotFOV.Value = Aimbot_FOV
            m3meBot.AimbotSmoothSpeed.Value = Aimbot_SmoothSpeed
            m3meBot.AimbotAutoShoot.Checked = Aimbot_AutoShoot
            m3meBot.TriggerEnabled.Checked = Trigger_Enabled
            m3meBot.TriggerBotKey.Text = Trigger_Key
            m3meBot.TriggerBotDelay.Text = Trigger_Delay
            m3meBot.GlowTeam.Checked = Glow_Team_Enabled
            m3meBot.GlowEnemy.Checked = Glow_Enemy_Enabled
            m3meBot.TeamRainbow.Checked = Glow_Team_Rainbow
            m3meBot.EnemyRainbow.Checked = Glow_Enemy_Rainbow
            m3meBot.GlowAlpha.Value = Glow_Alpha
            m3meBot.TeamR.Value = Glow_Team_R
            m3meBot.TeamG.Value = Glow_Team_G
            m3meBot.TeamB.Value = Glow_Team_B
            m3meBot.EnemyR.Value = Glow_Enemy_R
            m3meBot.EnemyG.Value = Glow_Enemy_G
            m3meBot.EnemyB.Value = Glow_Enemy_B
            m3meBot.BunnyHop.Checked = Misc_Bhop_Enabled
            m3meBot.RadarEnabled.Checked = Misc_Radar_Enabled
            m3meBot.NoFlashEnabled.Checked = Misc_NoFlash_Enabled
            'm3meBot.SlowAimEnabled.Checked = Misc_SlowAim_Enabled
            'm3meBot.SlowAimSensitivity.Text = Misc_SlowAim_Sensitivity
            m3meBot.RapidFireEnabled.Checked = Misc_RapidFire_Enabled
            m3meBot.RapidFireKey.Text = Misc_RapidFire_Key
        Catch ex As Exception
        End Try
    End Sub
End Module
