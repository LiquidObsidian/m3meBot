﻿Imports m3meLib

Public Class m3meBot
    Private Sub Initialize(sender As Object, e As EventArgs) Handles MyBase.Load
        Memory.Init()

        Config.Load()
        Config.UpdateMenu()
        Config.Save()

        Dim Threads() As Threading.Thread = {
            New Threading.Thread(AddressOf Bhop),
            New Threading.Thread(AddressOf Glow),
            New Threading.Thread(AddressOf Trigger),
            New Threading.Thread(AddressOf Radar),
            New Threading.Thread(AddressOf Aimbot),
            New Threading.Thread(AddressOf Rainbow),
            New Threading.Thread(AddressOf RapidFire),
            New Threading.Thread(AddressOf NoFlash)
        }

        For i = 0 To Threads.Length - 1
            Threads(i).Start()
        Next

        CheckForIllegalCrossThreadCalls = False
    End Sub

    Sub Bhop()
        Console.ForegroundColor = ConsoleColor.Gray
        Console.WriteLine("Bhop thread started.")
        While True
            Dim LocalPlayer = Client.GetLocalPlayer()
            Dim OnGround = LocalPlayer.IsOnGround()
            Dim HoldingSpace = Input.IsKeyDown(32)

            If OnGround And HoldingSpace And BunnyHop.Checked Then
                Client.Hop()
            Else
                Threading.Thread.Sleep(1)
            End If
        End While
    End Sub

    Sub Trigger()
        Console.ForegroundColor = ConsoleColor.Gray
        Console.WriteLine("Trigger thread started.")
        While True
            Dim LocalPlayer = Client.GetLocalPlayer()
            Dim HoldingTriggerKey = Input.IsKeyDown(Trigger_Key)

            If LocalPlayer.IsPlayerInCross() And HoldingTriggerKey And Trigger_Enabled Then
                Dim InCrossPlayer As Player = LocalPlayer.InCross()
                If Not (InCrossPlayer.Team() = LocalPlayer.Team()) Then
                    Threading.Thread.Sleep(Trigger_Delay)
                    Client.Tap()
                End If
            End If

            Threading.Thread.Sleep(1)
        End While
    End Sub

    Sub Radar()
        Console.ForegroundColor = ConsoleColor.Gray
        Console.WriteLine("Radar thread started.")
        While True
            If Misc_Radar_Enabled Then
                Dim Entities = Client.GetEntityList()

                For Each Ply As Player In Entities
                    If Not Ply.IsSpotted() And Not Ply.IsDormant() And RadarEnabled.Checked Then
                        Ply.SetSpotted(True)
                    End If
                Next
            End If

            Threading.Thread.Sleep(15)
        End While
    End Sub

    Sub Glow()
        Console.ForegroundColor = ConsoleColor.Gray
        Console.WriteLine("Glow thread started.")
        While True
            Dim LocalPlayer = Client.GetLocalPlayer()
            Dim Entities = Client.GetEntityList()

            If Glow_Enemy_Enabled Or Glow_Team_Enabled Then
                For Each Ply As Player In Entities
                    If Not Ply.IsDormant() Then
                        If Ply.Team() = LocalPlayer.Team() And Glow_Team_Enabled Then
                            If Glow_Team_Rainbow Then
                                Render.Glow(Ply, New Color(RainbowColor(0), RainbowColor(1), RainbowColor(2), Glow_Alpha))
                            Else
                                Render.Glow(Ply, New Color(Glow_Team_R, Glow_Team_G, Glow_Team_B, Glow_Alpha))
                            End If
                        End If
                        If Not (Ply.Team() = LocalPlayer.Team()) And Glow_Enemy_Enabled Then
                            If Glow_Enemy_Rainbow Then
                                Render.Glow(Ply, New Color(RainbowColor(0), RainbowColor(1), RainbowColor(2), Glow_Alpha))
                            Else
                                Render.Glow(Ply, New Color(Glow_Enemy_R, Glow_Enemy_G, Glow_Enemy_B, Glow_Alpha))
                            End If
                        End If
                    End If
                    Threading.Thread.Sleep(1)
                Next
            End If
            Threading.Thread.Sleep(15)
        End While
    End Sub

    Sub Aimbot()
        Console.ForegroundColor = ConsoleColor.Gray
        Console.WriteLine("Aimbot thread started.")
        While True
            Dim AimKey = Input.IsKeyDown(Aimbot_Key)
            If AimKey And Aimbot_Enabled Then
                Dim LocalPlayer As Player = Client.GetLocalPlayer()
                Try
                    Dim Player As Player = Engine.GetClosestPlayerToCross(Aimbot_FOV)
                    If Player.Exists() And Not Player.Team() = LocalPlayer.Team() Then
                        If Aimbot_Smooth Then
                            Engine.AimAtSmooth(Player, Bones.Head, AimbotSmoothSpeed.Value)

                            If Aimbot_AutoShoot And Engine.IsAimingAtHead(Player) Then
                                Client.Tap()
                            End If
                        Else
                            Engine.AimAt(Player, Bones.Head)

                            If Aimbot_AutoShoot Then
                                Client.Tap()
                            End If
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If

            Threading.Thread.Sleep(1)
        End While
    End Sub

    Dim Sensitivity As Single = 0
    'Sub SlowAim()
    '    While True
    '        Dim LocalPlayer As Player = Client.GetLocalPlayer()
    '        If LocalPlayer.IsPlayerInCross() Then
    '            Dim InCross = LocalPlayer.InCross()

    '            If Not InCross.Team() = LocalPlayer.Team() Then
    '                Client.SetSensitivity(Misc_SlowAim_Sensitivity)
    '            Else
    '                Client.SetSensitivity(Sensitivity)
    '            End If
    '        Else
    '            Client.SetSensitivity(Sensitivity)
    '        End If
    '        Threading.Thread.Sleep(1)
    '    End While
    'End Sub

    Sub RapidFire()
        While True
            Dim RapidFireKey = Input.IsKeyDown(Misc_RapidFire_Key)

            If RapidFireKey Then
                Client.Tap()
            End If

            Threading.Thread.Sleep(1)
        End While
    End Sub

    Public RainbowColor() As Integer = {255, 0, 0}
    Private RainbowSpeed As Integer = 1

    Sub Rainbow()
        While True
            For i As Integer = 1 To 255
                RainbowColor(2) += 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(0) -= 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(1) += 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(2) -= 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(0) += 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(1) -= 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
        End While
    End Sub

    Sub NoFlash()
        While True
            If Misc_NoFlash_Enabled Then
                Client.SetFlashAlpha(0.0F)
            Else
                Client.SetFlashAlpha(255.0F)
            End If
            Threading.Thread.Sleep(500)
        End While
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Config.Save()
    End Sub

    Private Sub ConfigTimer_Tick(sender As Object, e As EventArgs) Handles ConfigTimer.Tick
        Config.UpdateConfig()
    End Sub

    Private Sub ExitHack(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Environment.Exit(0)
    End Sub
End Class
