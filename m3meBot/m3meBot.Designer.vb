﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class m3meBot
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(m3meBot))
        Me.MiscTab = New System.Windows.Forms.TabPage()
        Me.RapidFireKey = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.RapidFireEnabled = New System.Windows.Forms.CheckBox()
        Me.NoFlashEnabled = New System.Windows.Forms.CheckBox()
        Me.RadarEnabled = New System.Windows.Forms.CheckBox()
        Me.BunnyHop = New System.Windows.Forms.CheckBox()
        Me.GlowESPTab = New System.Windows.Forms.TabPage()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GlowAlpha = New System.Windows.Forms.TrackBar()
        Me.EnemyRainbow = New System.Windows.Forms.CheckBox()
        Me.TeamRainbow = New System.Windows.Forms.CheckBox()
        Me.EnemyB = New System.Windows.Forms.TrackBar()
        Me.EnemyG = New System.Windows.Forms.TrackBar()
        Me.EnemyR = New System.Windows.Forms.TrackBar()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TeamB = New System.Windows.Forms.TrackBar()
        Me.TeamG = New System.Windows.Forms.TrackBar()
        Me.TeamR = New System.Windows.Forms.TrackBar()
        Me.GlowEnemy = New System.Windows.Forms.CheckBox()
        Me.GlowTeam = New System.Windows.Forms.CheckBox()
        Me.TriggerTab = New System.Windows.Forms.TabPage()
        Me.TriggerBotDelay = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TriggerBotKey = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TriggerEnabled = New System.Windows.Forms.CheckBox()
        Me.AimbotTab = New System.Windows.Forms.TabPage()
        Me.AimbotAutoShoot = New System.Windows.Forms.CheckBox()
        Me.AimbotKey = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.AimbotSmooth = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.AimbotSmoothSpeed = New System.Windows.Forms.TrackBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.AimbotFOV = New System.Windows.Forms.TrackBar()
        Me.AimbotEnabled = New System.Windows.Forms.CheckBox()
        Me.Tabs = New System.Windows.Forms.TabControl()
        Me.Save = New System.Windows.Forms.TabPage()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ConfigTimer = New System.Windows.Forms.Timer(Me.components)
        Me.MiscTab.SuspendLayout()
        Me.GlowESPTab.SuspendLayout()
        CType(Me.GlowAlpha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnemyB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnemyG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnemyR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeamB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeamG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeamR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TriggerTab.SuspendLayout()
        Me.AimbotTab.SuspendLayout()
        CType(Me.AimbotSmoothSpeed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AimbotFOV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tabs.SuspendLayout()
        Me.Save.SuspendLayout()
        Me.SuspendLayout()
        '
        'MiscTab
        '
        Me.MiscTab.BackColor = System.Drawing.Color.DodgerBlue
        Me.MiscTab.Controls.Add(Me.RapidFireKey)
        Me.MiscTab.Controls.Add(Me.Label9)
        Me.MiscTab.Controls.Add(Me.RapidFireEnabled)
        Me.MiscTab.Controls.Add(Me.NoFlashEnabled)
        Me.MiscTab.Controls.Add(Me.RadarEnabled)
        Me.MiscTab.Controls.Add(Me.BunnyHop)
        Me.MiscTab.Location = New System.Drawing.Point(4, 22)
        Me.MiscTab.Name = "MiscTab"
        Me.MiscTab.Size = New System.Drawing.Size(261, 191)
        Me.MiscTab.TabIndex = 2
        Me.MiscTab.Text = "Misc."
        '
        'RapidFireKey
        '
        Me.RapidFireKey.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RapidFireKey.Location = New System.Drawing.Point(220, 78)
        Me.RapidFireKey.MaximumSize = New System.Drawing.Size(30, 15)
        Me.RapidFireKey.MinimumSize = New System.Drawing.Size(30, 15)
        Me.RapidFireKey.Name = "RapidFireKey"
        Me.RapidFireKey.Size = New System.Drawing.Size(30, 13)
        Me.RapidFireKey.TabIndex = 9
        Me.RapidFireKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.Control
        Me.Label9.Location = New System.Drawing.Point(189, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(25, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Key"
        '
        'RapidFireEnabled
        '
        Me.RapidFireEnabled.AutoSize = True
        Me.RapidFireEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RapidFireEnabled.ForeColor = System.Drawing.SystemColors.Control
        Me.RapidFireEnabled.Location = New System.Drawing.Point(7, 76)
        Me.RapidFireEnabled.Name = "RapidFireEnabled"
        Me.RapidFireEnabled.Size = New System.Drawing.Size(72, 17)
        Me.RapidFireEnabled.TabIndex = 4
        Me.RapidFireEnabled.Text = "Rapid Fire"
        Me.RapidFireEnabled.UseVisualStyleBackColor = True
        '
        'NoFlashEnabled
        '
        Me.NoFlashEnabled.AutoSize = True
        Me.NoFlashEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.NoFlashEnabled.ForeColor = System.Drawing.SystemColors.Control
        Me.NoFlashEnabled.Location = New System.Drawing.Point(7, 53)
        Me.NoFlashEnabled.Name = "NoFlashEnabled"
        Me.NoFlashEnabled.Size = New System.Drawing.Size(66, 17)
        Me.NoFlashEnabled.TabIndex = 2
        Me.NoFlashEnabled.Text = "No Flash"
        Me.NoFlashEnabled.UseVisualStyleBackColor = True
        '
        'RadarEnabled
        '
        Me.RadarEnabled.AutoSize = True
        Me.RadarEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RadarEnabled.ForeColor = System.Drawing.SystemColors.Control
        Me.RadarEnabled.Location = New System.Drawing.Point(7, 30)
        Me.RadarEnabled.Name = "RadarEnabled"
        Me.RadarEnabled.Size = New System.Drawing.Size(53, 17)
        Me.RadarEnabled.TabIndex = 1
        Me.RadarEnabled.Text = "Radar"
        Me.RadarEnabled.UseVisualStyleBackColor = True
        '
        'BunnyHop
        '
        Me.BunnyHop.AutoSize = True
        Me.BunnyHop.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BunnyHop.ForeColor = System.Drawing.SystemColors.Control
        Me.BunnyHop.Location = New System.Drawing.Point(7, 7)
        Me.BunnyHop.Name = "BunnyHop"
        Me.BunnyHop.Size = New System.Drawing.Size(77, 17)
        Me.BunnyHop.TabIndex = 0
        Me.BunnyHop.Text = "Bunny Hop"
        Me.BunnyHop.UseVisualStyleBackColor = True
        '
        'GlowESPTab
        '
        Me.GlowESPTab.BackColor = System.Drawing.Color.DodgerBlue
        Me.GlowESPTab.Controls.Add(Me.Label13)
        Me.GlowESPTab.Controls.Add(Me.Label8)
        Me.GlowESPTab.Controls.Add(Me.Label7)
        Me.GlowESPTab.Controls.Add(Me.GlowAlpha)
        Me.GlowESPTab.Controls.Add(Me.EnemyRainbow)
        Me.GlowESPTab.Controls.Add(Me.TeamRainbow)
        Me.GlowESPTab.Controls.Add(Me.EnemyB)
        Me.GlowESPTab.Controls.Add(Me.EnemyG)
        Me.GlowESPTab.Controls.Add(Me.EnemyR)
        Me.GlowESPTab.Controls.Add(Me.Label6)
        Me.GlowESPTab.Controls.Add(Me.Label5)
        Me.GlowESPTab.Controls.Add(Me.Label4)
        Me.GlowESPTab.Controls.Add(Me.TeamB)
        Me.GlowESPTab.Controls.Add(Me.TeamG)
        Me.GlowESPTab.Controls.Add(Me.TeamR)
        Me.GlowESPTab.Controls.Add(Me.GlowEnemy)
        Me.GlowESPTab.Controls.Add(Me.GlowTeam)
        Me.GlowESPTab.Location = New System.Drawing.Point(4, 22)
        Me.GlowESPTab.Name = "GlowESPTab"
        Me.GlowESPTab.Padding = New System.Windows.Forms.Padding(3)
        Me.GlowESPTab.Size = New System.Drawing.Size(261, 191)
        Me.GlowESPTab.TabIndex = 1
        Me.GlowESPTab.Text = "Glow ESP"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Control
        Me.Label13.Location = New System.Drawing.Point(117, 58)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(39, 13)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Alpha"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.Control
        Me.Label8.Location = New System.Drawing.Point(191, 82)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Enemy"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.Control
        Me.Label7.Location = New System.Drawing.Point(85, 82)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Team"
        '
        'GlowAlpha
        '
        Me.GlowAlpha.Location = New System.Drawing.Point(156, 53)
        Me.GlowAlpha.Maximum = 255
        Me.GlowAlpha.Name = "GlowAlpha"
        Me.GlowAlpha.Size = New System.Drawing.Size(98, 45)
        Me.GlowAlpha.TabIndex = 15
        Me.GlowAlpha.Tag = "Red"
        Me.GlowAlpha.TickFrequency = 20
        '
        'EnemyRainbow
        '
        Me.EnemyRainbow.AutoSize = True
        Me.EnemyRainbow.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.EnemyRainbow.ForeColor = System.Drawing.SystemColors.Control
        Me.EnemyRainbow.Location = New System.Drawing.Point(157, 30)
        Me.EnemyRainbow.Name = "EnemyRainbow"
        Me.EnemyRainbow.Size = New System.Drawing.Size(101, 17)
        Me.EnemyRainbow.TabIndex = 14
        Me.EnemyRainbow.Text = "Enemy Rainbow"
        Me.EnemyRainbow.UseVisualStyleBackColor = True
        '
        'TeamRainbow
        '
        Me.TeamRainbow.AutoSize = True
        Me.TeamRainbow.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.TeamRainbow.ForeColor = System.Drawing.SystemColors.Control
        Me.TeamRainbow.Location = New System.Drawing.Point(157, 7)
        Me.TeamRainbow.Name = "TeamRainbow"
        Me.TeamRainbow.Size = New System.Drawing.Size(96, 17)
        Me.TeamRainbow.TabIndex = 13
        Me.TeamRainbow.Text = "Team Rainbow"
        Me.TeamRainbow.UseVisualStyleBackColor = True
        '
        'EnemyB
        '
        Me.EnemyB.Location = New System.Drawing.Point(157, 163)
        Me.EnemyB.Maximum = 255
        Me.EnemyB.Name = "EnemyB"
        Me.EnemyB.Size = New System.Drawing.Size(101, 45)
        Me.EnemyB.TabIndex = 10
        Me.EnemyB.TickFrequency = 20
        '
        'EnemyG
        '
        Me.EnemyG.Location = New System.Drawing.Point(157, 130)
        Me.EnemyG.Maximum = 255
        Me.EnemyG.Name = "EnemyG"
        Me.EnemyG.Size = New System.Drawing.Size(101, 45)
        Me.EnemyG.TabIndex = 9
        Me.EnemyG.TickFrequency = 20
        '
        'EnemyR
        '
        Me.EnemyR.Location = New System.Drawing.Point(157, 98)
        Me.EnemyR.Maximum = 255
        Me.EnemyR.Name = "EnemyR"
        Me.EnemyR.Size = New System.Drawing.Size(101, 45)
        Me.EnemyR.TabIndex = 8
        Me.EnemyR.Tag = "Red"
        Me.EnemyR.TickFrequency = 20
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Control
        Me.Label6.Location = New System.Drawing.Point(15, 167)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Blue"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Control
        Me.Label5.Location = New System.Drawing.Point(6, 135)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Green"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Control
        Me.Label4.Location = New System.Drawing.Point(17, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Red"
        '
        'TeamB
        '
        Me.TeamB.Location = New System.Drawing.Point(53, 163)
        Me.TeamB.Maximum = 255
        Me.TeamB.Name = "TeamB"
        Me.TeamB.Size = New System.Drawing.Size(98, 45)
        Me.TeamB.TabIndex = 4
        Me.TeamB.TickFrequency = 20
        '
        'TeamG
        '
        Me.TeamG.Location = New System.Drawing.Point(53, 130)
        Me.TeamG.Maximum = 255
        Me.TeamG.Name = "TeamG"
        Me.TeamG.Size = New System.Drawing.Size(98, 45)
        Me.TeamG.TabIndex = 3
        Me.TeamG.TickFrequency = 20
        '
        'TeamR
        '
        Me.TeamR.Location = New System.Drawing.Point(53, 98)
        Me.TeamR.Maximum = 255
        Me.TeamR.Name = "TeamR"
        Me.TeamR.Size = New System.Drawing.Size(98, 45)
        Me.TeamR.TabIndex = 2
        Me.TeamR.Tag = "Red"
        Me.TeamR.TickFrequency = 20
        '
        'GlowEnemy
        '
        Me.GlowEnemy.AutoSize = True
        Me.GlowEnemy.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GlowEnemy.ForeColor = System.Drawing.SystemColors.Control
        Me.GlowEnemy.Location = New System.Drawing.Point(7, 30)
        Me.GlowEnemy.Name = "GlowEnemy"
        Me.GlowEnemy.Size = New System.Drawing.Size(83, 17)
        Me.GlowEnemy.TabIndex = 1
        Me.GlowEnemy.Text = "Glow Enemy"
        Me.GlowEnemy.UseVisualStyleBackColor = True
        '
        'GlowTeam
        '
        Me.GlowTeam.AutoSize = True
        Me.GlowTeam.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GlowTeam.ForeColor = System.Drawing.SystemColors.Control
        Me.GlowTeam.Location = New System.Drawing.Point(7, 7)
        Me.GlowTeam.Name = "GlowTeam"
        Me.GlowTeam.Size = New System.Drawing.Size(78, 17)
        Me.GlowTeam.TabIndex = 0
        Me.GlowTeam.Text = "Glow Team"
        Me.GlowTeam.UseVisualStyleBackColor = True
        '
        'TriggerTab
        '
        Me.TriggerTab.BackColor = System.Drawing.Color.DodgerBlue
        Me.TriggerTab.Controls.Add(Me.TriggerBotDelay)
        Me.TriggerTab.Controls.Add(Me.Label11)
        Me.TriggerTab.Controls.Add(Me.TriggerBotKey)
        Me.TriggerTab.Controls.Add(Me.Label10)
        Me.TriggerTab.Controls.Add(Me.TriggerEnabled)
        Me.TriggerTab.Location = New System.Drawing.Point(4, 22)
        Me.TriggerTab.Name = "TriggerTab"
        Me.TriggerTab.Size = New System.Drawing.Size(261, 191)
        Me.TriggerTab.TabIndex = 3
        Me.TriggerTab.Text = "Trigger"
        '
        'TriggerBotDelay
        '
        Me.TriggerBotDelay.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TriggerBotDelay.Location = New System.Drawing.Point(8, 49)
        Me.TriggerBotDelay.Name = "TriggerBotDelay"
        Me.TriggerBotDelay.Size = New System.Drawing.Size(30, 13)
        Me.TriggerBotDelay.TabIndex = 11
        Me.TriggerBotDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.Control
        Me.Label11.Location = New System.Drawing.Point(44, 49)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Delay"
        '
        'TriggerBotKey
        '
        Me.TriggerBotKey.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TriggerBotKey.Location = New System.Drawing.Point(8, 30)
        Me.TriggerBotKey.Name = "TriggerBotKey"
        Me.TriggerBotKey.Size = New System.Drawing.Size(30, 13)
        Me.TriggerBotKey.TabIndex = 9
        Me.TriggerBotKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.Control
        Me.Label10.Location = New System.Drawing.Point(44, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(25, 13)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Key"
        '
        'TriggerEnabled
        '
        Me.TriggerEnabled.AutoSize = True
        Me.TriggerEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.TriggerEnabled.ForeColor = System.Drawing.SystemColors.Control
        Me.TriggerEnabled.Location = New System.Drawing.Point(7, 7)
        Me.TriggerEnabled.Name = "TriggerEnabled"
        Me.TriggerEnabled.Size = New System.Drawing.Size(63, 17)
        Me.TriggerEnabled.TabIndex = 1
        Me.TriggerEnabled.Text = "Enabled"
        Me.TriggerEnabled.UseVisualStyleBackColor = True
        '
        'AimbotTab
        '
        Me.AimbotTab.BackColor = System.Drawing.Color.DodgerBlue
        Me.AimbotTab.Controls.Add(Me.AimbotAutoShoot)
        Me.AimbotTab.Controls.Add(Me.AimbotKey)
        Me.AimbotTab.Controls.Add(Me.Label3)
        Me.AimbotTab.Controls.Add(Me.AimbotSmooth)
        Me.AimbotTab.Controls.Add(Me.Label2)
        Me.AimbotTab.Controls.Add(Me.AimbotSmoothSpeed)
        Me.AimbotTab.Controls.Add(Me.Label1)
        Me.AimbotTab.Controls.Add(Me.AimbotFOV)
        Me.AimbotTab.Controls.Add(Me.AimbotEnabled)
        Me.AimbotTab.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AimbotTab.Location = New System.Drawing.Point(4, 22)
        Me.AimbotTab.Name = "AimbotTab"
        Me.AimbotTab.Padding = New System.Windows.Forms.Padding(3)
        Me.AimbotTab.Size = New System.Drawing.Size(261, 191)
        Me.AimbotTab.TabIndex = 0
        Me.AimbotTab.Text = "Aimbot"
        '
        'AimbotAutoShoot
        '
        Me.AimbotAutoShoot.AutoSize = True
        Me.AimbotAutoShoot.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AimbotAutoShoot.ForeColor = System.Drawing.SystemColors.Control
        Me.AimbotAutoShoot.Location = New System.Drawing.Point(6, 53)
        Me.AimbotAutoShoot.Name = "AimbotAutoShoot"
        Me.AimbotAutoShoot.Size = New System.Drawing.Size(132, 17)
        Me.AimbotAutoShoot.TabIndex = 8
        Me.AimbotAutoShoot.Text = "Auto Shoot (head only)"
        Me.AimbotAutoShoot.UseVisualStyleBackColor = True
        '
        'AimbotKey
        '
        Me.AimbotKey.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.AimbotKey.Location = New System.Drawing.Point(107, 9)
        Me.AimbotKey.Name = "AimbotKey"
        Me.AimbotKey.Size = New System.Drawing.Size(30, 13)
        Me.AimbotKey.TabIndex = 7
        Me.AimbotKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.Control
        Me.Label3.Location = New System.Drawing.Point(76, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Key"
        '
        'AimbotSmooth
        '
        Me.AimbotSmooth.AutoSize = True
        Me.AimbotSmooth.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AimbotSmooth.ForeColor = System.Drawing.SystemColors.Control
        Me.AimbotSmooth.Location = New System.Drawing.Point(7, 30)
        Me.AimbotSmooth.Name = "AimbotSmooth"
        Me.AimbotSmooth.Size = New System.Drawing.Size(60, 17)
        Me.AimbotSmooth.TabIndex = 5
        Me.AimbotSmooth.Text = "Smooth"
        Me.AimbotSmooth.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.Control
        Me.Label2.Location = New System.Drawing.Point(7, 125)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Smooth Speed"
        '
        'AimbotSmoothSpeed
        '
        Me.AimbotSmoothSpeed.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AimbotSmoothSpeed.Location = New System.Drawing.Point(8, 144)
        Me.AimbotSmoothSpeed.Maximum = 20
        Me.AimbotSmoothSpeed.Minimum = 1
        Me.AimbotSmoothSpeed.Name = "AimbotSmoothSpeed"
        Me.AimbotSmoothSpeed.Size = New System.Drawing.Size(247, 45)
        Me.AimbotSmoothSpeed.TabIndex = 3
        Me.AimbotSmoothSpeed.Value = 10
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.Control
        Me.Label1.Location = New System.Drawing.Point(7, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "FOV:"
        '
        'AimbotFOV
        '
        Me.AimbotFOV.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AimbotFOV.Location = New System.Drawing.Point(8, 93)
        Me.AimbotFOV.Maximum = 360
        Me.AimbotFOV.Name = "AimbotFOV"
        Me.AimbotFOV.Size = New System.Drawing.Size(247, 45)
        Me.AimbotFOV.TabIndex = 1
        Me.AimbotFOV.TickFrequency = 5
        Me.AimbotFOV.Value = 45
        '
        'AimbotEnabled
        '
        Me.AimbotEnabled.AutoSize = True
        Me.AimbotEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AimbotEnabled.ForeColor = System.Drawing.SystemColors.Control
        Me.AimbotEnabled.Location = New System.Drawing.Point(7, 7)
        Me.AimbotEnabled.Name = "AimbotEnabled"
        Me.AimbotEnabled.Size = New System.Drawing.Size(63, 17)
        Me.AimbotEnabled.TabIndex = 0
        Me.AimbotEnabled.Text = "Enabled"
        Me.AimbotEnabled.UseVisualStyleBackColor = True
        '
        'Tabs
        '
        Me.Tabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tabs.Controls.Add(Me.AimbotTab)
        Me.Tabs.Controls.Add(Me.TriggerTab)
        Me.Tabs.Controls.Add(Me.GlowESPTab)
        Me.Tabs.Controls.Add(Me.MiscTab)
        Me.Tabs.Controls.Add(Me.Save)
        Me.Tabs.Location = New System.Drawing.Point(0, 0)
        Me.Tabs.Name = "Tabs"
        Me.Tabs.SelectedIndex = 0
        Me.Tabs.Size = New System.Drawing.Size(269, 217)
        Me.Tabs.TabIndex = 0
        '
        'Save
        '
        Me.Save.BackColor = System.Drawing.Color.DodgerBlue
        Me.Save.Controls.Add(Me.SaveButton)
        Me.Save.Location = New System.Drawing.Point(4, 22)
        Me.Save.Name = "Save"
        Me.Save.Padding = New System.Windows.Forms.Padding(3)
        Me.Save.Size = New System.Drawing.Size(261, 191)
        Me.Save.TabIndex = 4
        Me.Save.Text = "Save"
        '
        'SaveButton
        '
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SaveButton.ForeColor = System.Drawing.SystemColors.Control
        Me.SaveButton.Location = New System.Drawing.Point(6, 6)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(248, 178)
        Me.SaveButton.TabIndex = 0
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'ConfigTimer
        '
        Me.ConfigTimer.Enabled = True
        Me.ConfigTimer.Interval = 50
        '
        'm3meBot
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(270, 218)
        Me.Controls.Add(Me.Tabs)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "m3meBot"
        Me.Text = "m3meBot"
        Me.MiscTab.ResumeLayout(False)
        Me.MiscTab.PerformLayout()
        Me.GlowESPTab.ResumeLayout(False)
        Me.GlowESPTab.PerformLayout()
        CType(Me.GlowAlpha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnemyB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnemyG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnemyR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeamB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeamG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeamR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TriggerTab.ResumeLayout(False)
        Me.TriggerTab.PerformLayout()
        Me.AimbotTab.ResumeLayout(False)
        Me.AimbotTab.PerformLayout()
        CType(Me.AimbotSmoothSpeed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AimbotFOV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tabs.ResumeLayout(False)
        Me.Save.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MiscTab As System.Windows.Forms.TabPage
    Friend WithEvents RapidFireKey As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents RapidFireEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents NoFlashEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents RadarEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents BunnyHop As System.Windows.Forms.CheckBox
    Friend WithEvents GlowESPTab As System.Windows.Forms.TabPage
    Friend WithEvents EnemyRainbow As System.Windows.Forms.CheckBox
    Friend WithEvents TeamRainbow As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents EnemyB As System.Windows.Forms.TrackBar
    Friend WithEvents EnemyG As System.Windows.Forms.TrackBar
    Friend WithEvents EnemyR As System.Windows.Forms.TrackBar
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TeamB As System.Windows.Forms.TrackBar
    Friend WithEvents TeamG As System.Windows.Forms.TrackBar
    Friend WithEvents TeamR As System.Windows.Forms.TrackBar
    Friend WithEvents GlowEnemy As System.Windows.Forms.CheckBox
    Friend WithEvents GlowTeam As System.Windows.Forms.CheckBox
    Friend WithEvents TriggerTab As System.Windows.Forms.TabPage
    Friend WithEvents TriggerBotDelay As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TriggerBotKey As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TriggerEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents AimbotTab As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents AimbotSmooth As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents AimbotSmoothSpeed As System.Windows.Forms.TrackBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents AimbotFOV As System.Windows.Forms.TrackBar
    Friend WithEvents AimbotEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents Tabs As System.Windows.Forms.TabControl
    Friend WithEvents AimbotKey As System.Windows.Forms.TextBox
    Friend WithEvents ConfigTimer As System.Windows.Forms.Timer
    Friend WithEvents Save As System.Windows.Forms.TabPage
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GlowAlpha As System.Windows.Forms.TrackBar
    Friend WithEvents AimbotAutoShoot As System.Windows.Forms.CheckBox

End Class
